# lib_mqtt

#### 介绍
使用paho mqtt C 库进行二次封装简单易用的mqtt客户端库。其中包含了paho的源码以及二次封装库的源码。另外增加了一个调用例程。
paho.mqtt.c为paho的源码，可以使用Cmake进行配置 并编译
编译流程：
X86不带SSL，静态库
1.cmake . -DPAHO_BUILD_STATIC=TRUE -DCMAKE_C_COMPILER=gcc -DPAHO_WITH_SSL=FALSE
2.make
3.在src能找到两个库

ARM
1.cmake . -DPAHO_BUILD_STATIC=TRUE -DCMAKE_C_COMPILER=arm-linux-gnueabihf-gcc -DPAHO_WITH_SSL=FALSE
2.make
3.在src能找到两个库

mqtt_lib为二次封装库的源码
mqtt_lib_test为调用例程。我这里编译是Ubuntu下X86的库和程序。如果使用交叉编译的话需要交叉编译上述两个库

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
